terraform {
 backend "gcs" {
   bucket  = "${var.user}-terraform-admin"
   prefix  = "terraform/state"
 }
}
