# Managing GCP projects with terraform

## Todo
I want modules

## Setup

![alt text](https://storage.googleapis.com/gcp-community/tutorials/managing-gcp-projects-with-terraform/diagram.png)

env:

```bash
#Export the following variables to your environment for use throughout:
export TF_VAR_org_id=XXXXXXXXXX
export TF_VAR_billing_account=XXXXXX-XXXXXX-XXXXXX
export TF_ADMIN=${USER}-terraform-admin
export TF_CREDS=~/.config/gcloud/${USER}-terraform-admin.json
#Configure your environment for the Google Cloud Terraform provider:
export GOOGLE_APPLICATION_CREDENTIALS=${TF_CREDS}
export GOOGLE_PROJECT=${TF_ADMIN}
#Set the name of the project you want to create and the region you want to create the resources in:
export TF_VAR_project_name=${USER}-test-compute
export TF_VAR_region=europe-west4
export TF_VAR_user_access=user@domain.com
export TF_VAR_user=${USER}
```

```bash
source .env
```

Create terraform admin project:

```google cloud
gcloud projects create ${TF_ADMIN} \
  --organization ${TF_VAR_org_id} \
  --set-as-default
  
gcloud beta billing projects link ${TF_ADMIN} \
  --billing-account ${TF_VAR_billing_account}
 ```
 
 Create the service account in the Terraform admin project and download the JSON credentials:
 ```google cloud
gcloud iam service-accounts create terraform \
  --display-name "Terraform admin account"

gcloud iam service-accounts keys create ${TF_CREDS} \
  --iam-account terraform@${TF_ADMIN}.iam.gserviceaccount.com
```

Grant the service account permission to view the Admin Project and manage Cloud Storage:
```google cloud
gcloud projects add-iam-policy-binding ${TF_ADMIN} \
  --member serviceAccount:terraform@${TF_ADMIN}.iam.gserviceaccount.com \
  --role roles/viewer

gcloud projects add-iam-policy-binding ${TF_ADMIN} \
  --member serviceAccount:terraform@${TF_ADMIN}.iam.gserviceaccount.com \
  --role roles/storage.admin
```

Any actions that Terraform performs require that the API be enabled to do so. In this guide, Terraform requires the following:
```google cloud
gcloud services enable cloudresourcemanager.googleapis.com
gcloud services enable cloudbilling.googleapis.com
gcloud services enable iam.googleapis.com
gcloud services enable compute.googleapis.com
gcloud services enable serviceusage.googleapis.com
```

Grant the service account permission to create projects and assign billing accounts:
```google cloud
gcloud organizations add-iam-policy-binding ${TF_VAR_org_id} \
  --member serviceAccount:terraform@${TF_ADMIN}.iam.gserviceaccount.com \
  --role roles/resourcemanager.projectCreator

gcloud organizations add-iam-policy-binding ${TF_VAR_org_id} \
  --member serviceAccount:terraform@${TF_ADMIN}.iam.gserviceaccount.com \
  --role roles/billing.user
```

Create the remote backend bucket in Cloud Storage and the backend.tf file for storage of the terraform.tfstate file:
```google cloud
gsutil mb -p ${TF_ADMIN} gs://${TF_ADMIN}
```

Enable versioning for said remote bucket:
```google cloud
gsutil versioning set on gs://${TF_ADMIN}
```

Terraform:
```google cloud
terraform init
terraform apply
```

SSH into the instance created:
```google cloud
export instance_id=$(terraform output instance_id)
export project_id=$(terraform output project_id)

gcloud compute ssh ${instance_id} --project ${project_id}
```
